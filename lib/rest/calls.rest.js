import axios from 'axios';

// Shouldn't call axios get but jlFetch or something
// Should probably go in a utils folder/file
export const axiosGet = async (url, options = {}) => {
	const res = await axios.get(url, options);

	return res.data;
};
