import Layout from '../components/layout/layout';
import '../styles/globals.scss';
import PropTypes from 'prop-types';

function MyApp({Component, pageProps}) {
	return (
		<Layout>
			<Component {...pageProps} />
		</Layout>
	);
}

export default MyApp;

MyApp.propTypes = {
	Component: PropTypes.element,
	pageProps: PropTypes.object,
};
