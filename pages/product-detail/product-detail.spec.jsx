import {render, screen} from '@testing-library/react';
import ProductDetail, {getStaticProps} from './[id]';
import '@testing-library/jest-dom';
import axios from 'axios';
import productsData from '../../mockData/data2.json';
import {mockUseRouter} from '../../__mocks__';

const exampleData = productsData.detailsData[0];
const context = {params: {id: '3218074'}};
const expectedProduct = {
	media: exampleData.media,
	details: exampleData.details,
	title: exampleData.title,
	price: exampleData.price,
	displaySpecialOffer: exampleData.displaySpecialOffer,
	additionalServices: exampleData.additionalServices,
	code: exampleData.code,
};
jest.mock('axios');

jest.mock('isomorphic-dompurify', () => ({sanitize: txt => txt}));

jest.mock('next/router', () => ({
	useRouter() {
		return mockUseRouter();
	},
}));

let data = null;

describe('Details Page getStaticProps', () => {
	beforeEach(async () => {
		axios.get.mockResolvedValue({data: exampleData});
		const {props} = await getStaticProps(context);
		data = props.data;
	});
	afterEach(() => {
		data = null;
	});
	it('getStaticProps returns data and a list of items', async () => {
		expect(data).toBeDefined();
	});

	it('returns expected product data ', async () => {
		expect(data).toEqual(expectedProduct);
	});
});

describe('Details Page getStaticPaths', () => {
	it.todo('Test getStaticPaths - is this possible?');
});

describe('Details Page data', () => {
	beforeEach(async () => {
		axios.get.mockResolvedValue({data: exampleData});
		const {props} = await getStaticProps(context);
		render(<ProductDetail data={props.data} />);
	});

	it('it displays title', () => {
		const header = exampleData.title;
		expect(screen.getByText(header)).toBeDefined();
	});
});
