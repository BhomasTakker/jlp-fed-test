import PropTypes from 'prop-types';
import SiteHeader from '../../components/site-header/site-header';
import {axiosGet} from '../../lib/rest';
import styles from './product-detail.module.scss';
import PageHeader from '../../components/page-header/page-header';
import ProductInformation from '../../components/product-information/product-information';
import ProductSpecification from '../../components/product-specifiction/product-specification';
import ProductDisplay from '../../components/product-display/product-display';
import ProductPricing from '../../components/product-pricing/product-pricing';

const dataFilter = [
	'media',
	'details',
	'title',
	'price',
	'code',
	'displaySpecialOffer',
	'additionalServices',
];

const productListUrl = 'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI';

// Utils and units
const filterData = (data, dataFilter) => {
	const trimmedData = {};
	dataFilter.forEach(key => {
		trimmedData[key] = data[key];
	});

	return trimmedData;
};

export async function getStaticProps(context) {
	const {id} = context.params;
	let data;
	try {
		data = await axiosGet(
			'https://api.johnlewis.com/mobile-apps/api/v1/products/' + id,
		);
	} catch (err) {
		data = null;
	}

	if (!data) {
		return {
			notFound: true,
		};
	}

	return {
		props: {
			data: filterData(data, dataFilter),

		},
	};
}

const formPaths = ary => ary.map(prod => ({
	params: {id: prod.productId},
}));

// Need to get a list of the most popular products and pre render those prehaps
// Instead of pe render all or rendering none
// Hard coding this id/list is not really okay!
export async function getStaticPaths() {
	let data;
	try {
		data = await axiosGet(
			productListUrl,
		);
	} catch (err) {
		data = null;
	}

	const paths = formPaths(data.products);

	return {
		// Paths: [
		// 	{params: {id: '5561997'}},
		// ],
		paths,
		fallback: true,
	};
}

// Need to add a 'fallback' render for a loading moment
const ProductDetail = ({data}) => {
	if (!data) {
		return (
			<div>Loading...</div>
		);
	}

	const {details, title, price, displaySpecialOffer, additionalServices, code} = data;
	const {productInformation} = details;
	// Let displayInfo = new DOMParser().parseFromString(productInformation, 'text/html');
	return (
		<>
			<SiteHeader pageTitle={title} />
			<div>
				<PageHeader title={title}/>
				<main>
					{/* Create a grid of these for layout - 1234
					1 2
					3
					4  */}
					<div className={styles.grid}>
						{/* multiple classNames?  */}
						<div className={styles.gridItem1}>
							<ProductDisplay data={data} />
						</div>
						<div className={styles.gridItem2}>
							<ProductPricing price={price} displaySpecialOffer={displaySpecialOffer} additionalServices={additionalServices} />
						</div>
						<div className={styles.gridItem3}>
							<ProductInformation productInformation={productInformation} productCode={code} />
						</div>
						<div className={styles.gridItem4}>
							<ProductSpecification attributes={details.features[0].attributes}/>
						</div>
					</div>
				</main>
			</div>
		</>
	);
};

ProductDetail.propTypes = {
	data: PropTypes.shape({
		details: PropTypes.object.isRequired,
		title: PropTypes.string.isRequired,
		price: PropTypes.object.isRequired,
		displaySpecialOffer: PropTypes.string.isRequired,
		additionalServices: PropTypes.object.isRequired,
		code: PropTypes.string.isRequired,
	}).isRequired,
};

export default ProductDetail;
