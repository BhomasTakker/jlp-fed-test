import {render, screen} from '@testing-library/react';
import Home, {getStaticProps} from './index';
import '@testing-library/jest-dom';
import productsListData from '../mockData/data.json';
import axios from 'axios';
import {mockUseRouter} from '../__mocks__';

const exampleProduct = productsListData.products[0];
//
exampleProduct.variantPriceRange = {
	display: {
		max: '£500.00',
		min: '£500.00',
	},
	value: {
		max: '500.00',
		min: '500.00',
	},
};

jest.mock('axios');

jest.mock('next/router', () => ({
	useRouter() {
		return mockUseRouter();
	},
}));

describe('Home Page getStaticProps', () => {
	beforeEach(() => {
		axios.get.mockResolvedValue({data: productsListData});
	});
	it('getStaticProps returns a list of items', async () => {
		const {props} = await getStaticProps({});
		const {products} = props;

		// Because we define the shape of this mock data we probably don't need to check that?
		expect(products.length).toBeGreaterThan(0);// Not required
	});
	it('it returns products array', async () => {
		const {props} = await getStaticProps({});
		const {products} = props;

		expect(products.length).toBeGreaterThan(0);
	});
	it('it returns only required product data', async () => {
		const {props} = await getStaticProps({});
		const {products} = props;
		const expectedProduct = {
			productId: exampleProduct.productId,
			image: exampleProduct.image,
			title: exampleProduct.title,
			variantPriceRange: exampleProduct.variantPriceRange,
		};

		expect(products[0]).toEqual(expectedProduct);
	});
});

describe('Home Page data ', () => {
	beforeEach(async () => {
		axios.get.mockResolvedValue({data: productsListData});
		const {props} = await getStaticProps({});
		render(<Home products={props.products} />);
	});

	it('renders a header of Dishwashers with number of products', () => {
		const length = 20;
		const header = `Dishwashers (${length})`;

		expect(screen.getByText(header)).toBeDefined();
	});

	it('It renders a list of items', async () => {
		const testid = 'list-item';
		const expected = 20;
		const elements = screen.getAllByTestId(testid);

		expect(elements.length).toEqual(expected);
	});

	it('item has a title', () => {
		const {title} = exampleProduct;

		const element = screen.getByText(title);

		expect(element).toBeDefined();
	});
	it('item has a price', () => {
		const price = exampleProduct.variantPriceRange.display.max;
		const elements = screen.getAllByText(price);

		expect(elements[0]).toBeDefined();
	});
});
