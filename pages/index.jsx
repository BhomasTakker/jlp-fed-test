import styles from './index.module.scss';
import PropTypes from 'prop-types';
import {axiosGet} from '../lib/rest';
import SiteHeader from '../components/site-header/site-header';
import PageHeader from '../components/page-header/page-header';
import ProductList from '../components/product-list/product-list';

const url = 'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI';

const trimProductData = products => products.map(product => ({
	productId: product.productId,
	image: product.image,
	variantPriceRange: {...product.variantPriceRange},
	title: product.title,
}));

const reduceProducts = (products, n) => products.slice(0, n);

export async function getStaticProps() {
	let data;
	try {// Should 'always' try catch an await
		data = await axiosGet(url);// For neatness, abstraction, reuse - if change from using axios to fetch one place to update - okay the name is wrong!
	} catch (err) {
		data = null;
		console.error(err);
	}

	if (!data) {
		return {redirect: '/404'};
	}

	const useProducts = reduceProducts(data.products, 20);

	// Should prune return of any non required data
	return {
		props: {
			products: trimProductData(useProducts),
		},
		revalidate: 30, // Regenerate this page every thirty seconds or request - whichever is the longer
	};
}

const Home = ({products}) => {
	const items = products || []; // Protect against missing parameter / or deal withh it
	return (
		<div>
			<SiteHeader pageTitle='Dishwashers'/>
			<div>
				<PageHeader title={`Dishwashers (${items.length})`} />
				<main>
					<ProductList items={items}/>
				</main>
			</div>
		</div>
	);
};

// If not using TypeScript, and maybe if you are, propTypes to inform and enforce correct props for component
Home.propTypes = {
	products: PropTypes.array.isRequired,
};

export default Home;
