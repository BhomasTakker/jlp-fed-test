module.exports = {
	images: {
		domains: ['johnlewis.scene7.com'],
	},
	i18n: {
		locales: ['en'],
		defaultLocale: 'en',
	},
};
