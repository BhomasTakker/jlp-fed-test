# My-assumptions and notes

I went wildly over time on this.
In the end I just treated it as an exercise, what would I improve and how.

I had to look up a few things, refresh myself, and got tripped up a copuple of times, not everything has been fixed but I think I spotted the majority of issues.

My aproach really wasn't a TDD one, not at first, but if you follow the committs you'll see where I properly started. In hindsight I would do this differently but I think I just got lost in the issues.

For the most part it was quite enjoyable, good practice, and I learned a thing or two.

I would refractor a few things.
I know that the Headers (site and page) could be put into the layout component, there may be a couple of things to look up.
As is I'd remove that as it's largely redundant.
I think there was another component that cpuld be removed and one or two that could be broken up a little.
I needed to add click finctionality to some tests
Expand on tests elsewhere
Extend on some test suites, etc
I would clean up the css some more

Accessibility needs some work
I have tried to convert to some semantic html but it probably isn't quite far enough and it would need a good going over in that regard.

Security - there may be things I didn't spot or don't know here.
Inner html data gets purified and I ended up trimming returned server data.

Performance
I know next Images needed some work - but that would likely need to be a bit of a deep dive.

I probably should have dynamically loaded all available routes into static paths to be pre built but that decision changes with more data or updated data so what is the correct answer.

I added this.
Also added a notFound to redirect - 404 - if incorrect id provided

So you should be able to view details page for the four products not shown on the homepage

Some functions etc should probably go into a utils directory
I've largely noted when

I was also going to add in some storybook components but that wasn't going to be as easy as I hoped!

Okay, I didn't touch the 404 page - it needs some tests.
I'm sure there's a couple of gotchas in there but I'm not looking

(I still didn't even get to fonts)

///////////////////////////

NPM - Broken setup
node-sass is deprecated
Replace with sass

node-fetch is redundant in this instance - next allows fetch - also it's not uet released in LTS
incidentally I had a problem with fetch assuming headers and used axios
Will take another look if I get time

lodash was unused in the code so have removed
(In a previous company we were removing lodash - I think for performance but I'm not too sure)
N.B. As a general rule I wouldn't add/remove packages

versions for next, react, react-dom, testing library
all out of date

I have made the fairly wild assumption that all out of date packages should be updated to their latest versions

Instructions state port used as 3000 when it is hosted on 3001 - have changed to 3000

PropTypes was unused - I normally don't use PropTypes (TypeScript) but figured I should today
Some form of prop validation should be required

eslint added to project to enforce some basic rules
Have disabled react in jsx scope because using Next and React is always in scope
Should be some company coding standards enforced by eslint, etc

I made the decision, perhaps incorrectly, to get the page loading without error before adding tests and starting a more TDD development approach

N.B. This is the point of my first commit
Some fixes were ineveitable by this point just getting the code to load/run

# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy.

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md. (Gitlab...)
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed Node.js using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.
