import {jest} from '@jest/globals';

export const mockUseRouter = (route = '/', pathname = '/', query = '', asPath = '') => ({
	route,
	pathname,
	query,
	asPath,
	push: jest.fn(),
	events: {
		on: jest.fn(),
		off: jest.fn(),
	},
	beforePopState: jest.fn(() => null),
	prefetch: jest.fn(() => null),
});
