import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsData from '../../mockData/data2.json';
import ProductCarousel from './product-carousel';

const exampleData = productsData.detailsData[0];
const {media} = exampleData;
const {images} = media;
const {urls, altText} = images;

describe('Product Carousel', () => {
	beforeEach(() => {
		render(<ProductCarousel images={images} />);
	});

	it('displays image ', () => {
		// Testing for actual image returned blob...
		expect(screen.getByTestId('image')).toHaveAttribute('src');
	});
	it('has alt text ', () => {
		expect(screen.getByAltText(altText)).toBeDefined();
	});
	it('has correct number of image buttons ', () => {
		const testId = 'dot';
		const urlsLength = urls.length;
		expect(screen.getAllByTestId(testId).length).toEqual(urlsLength);
	});
	it.todo('image change on click ');
});
