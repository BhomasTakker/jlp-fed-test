import PropTypes from 'prop-types';
import Image from 'next/image';
import styles from './product-carousel.module.scss';
import {useState} from 'react';

// Think about splitting this component up

const getImageIndex = (n, len) => {
	const isEqual = n === (len - 1);
	return isEqual ? 0 : n + 1;
};

const createDots = (ary, hnd, selected) => (
	<div className={styles.container}>
		{
			ary.map((item, i) => {
				const isSelected = selected === i;
				const useClass = isSelected ? styles.dotActive : styles.dotInactive;
				return (
					<span
						className={useClass}
						role='button'
						label={`Image select ${i}`}
						key={`Image select ${i}`}
						data-testid='dot'
						onClick={() => hnd(i)}
					/>);
			})
		}
	</div>
);

const ProductCarousel = ({images}) => {
	const {urls, altText} = images;
	const [currentImageId, setCurrentImageId] = useState(0);

	const onClickHandler = () => {
		const imageId = getImageIndex(currentImageId, urls.length);
		setCurrentImageId(imageId);
	};

	const btnHandler = imageId => setCurrentImageId(imageId);

	return (

		// Need to convert to actual button
		// I believe a is specifically for navigation
		<div className={styles.productCarousel}>
			{/* Image - use pre set sizes for performance, etc.
			  I need to take a deeper dive into next/image as I really didn't know enough
				max-width: 500
	  */}
			<div onClick={onClickHandler} role='button'>
				<Image src={`https:${urls[currentImageId]}`}
					data-testid='image'
					alt={altText}
					width={480}
					height={640}
					layout='intrinsic'
					objectFit={'contain'} />
			</div>
			{createDots(urls, btnHandler, currentImageId)}
		</div>
	);
};

export default ProductCarousel;

ProductCarousel.propTypes = {
	images: PropTypes.shape({
		urls: PropTypes.array.isRequired,
		altText: PropTypes.string.isRequired,
	}).isRequired,
};
