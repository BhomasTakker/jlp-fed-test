import Link from 'next/link';
import PropTypes from 'prop-types';
import ProductListItem from '../product-list-item/product-list-item';
import styles from './product-list.module.scss';

// Should as part of ListItem
// List should only focus on aspects of list
const createItem = item => (
	<Link
		key={item.productId}
		href={{
			pathname: '/product-detail/[id]',
			query: {id: item.productId},
		}}
	>
		<a className={styles.link}>
			<ProductListItem image={item.image} price={item.variantPriceRange} title={item.title}/>
		</a>
	</Link>
);

const createList = items => items.map(item => (
	createItem(item)
));

const ProductList = ({items}) => (
	<section className={styles.content}>
		{createList(items)}
	</section>
);

ProductList.propTypes = {
	items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductList;
