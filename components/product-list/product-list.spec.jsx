import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsListData from '../../mockData/data.json';
import ProductList from './product-list';

const {products} = productsListData;
const exampleProduct = productsListData.products[0];

describe('ProductList ', () => {
	beforeEach(() => {
		render(<ProductList items={products}/>);
	});
	it('Renders list', () => {
		const role = 'link';
		const expectedNumber = products.length;
		const links = screen.getAllByRole(role).length;

		expect(links).toEqual(expectedNumber);
	});
	it('Renders expected item', () => {
		const {title} = exampleProduct;
		const element = screen.getByText(title);

		expect(element).toBeDefined();
	});
});
