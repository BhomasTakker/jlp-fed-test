import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import SpecificationObject from './specification-object';

const input = {
	name: 'input name',
	value: 'Value',
};

describe('SpecificationObject ', () => {
	beforeEach(() => {
		render(<SpecificationObject item={input}/>);
	});
	it('renders specification type', () => {
		const element = screen.getByText(input.name);
		expect(element).toBeDefined();
	});
	it('renders specification value', () => {
		const element = screen.getByText(input.value);
		expect(element).toBeDefined();
	});
});
