import PropTypes from 'prop-types';
import styles from './specification-object.module.scss';

const SpecificationObject = ({item}) => (
	<li key={item.name} className={styles.listItem} data-testid='specification-object'>
		<p>{item.name}</p>
		<p>{item.value}</p>
	</li>);

export default SpecificationObject;

SpecificationObject.propTypes = {
	item: PropTypes.object.isRequired,
};
