import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsData from '../../mockData/data2.json';
import ProductSpecification from './product-specification';

const {details} = productsData.detailsData[0];
const {features} = details;
// Is the form of mock Data incorrect?
const {attributes} = features[0];

const fakeList = [
	{
		name: 'delay start',
		value: 'value'},
	{
		name: 'child lock',
		corruptvalue: 'value',
	},
];

const listFilter = [
	'adjustable racking',
	'child lock',
	'delay start',
	'delicate wash',
	'dimensions',
	'drying performance',
	'drying system',

];

describe('ProductSpecification ', () => {
	it('it lists expected number of items', () => {
		render(<ProductSpecification attributes={attributes}/>);
		const testId = 'specification-object';
		const filteredList = attributes.filter(item => listFilter.includes(item.name.toLowerCase()));
		const expected = filteredList.length;
		const elements = screen.getAllByTestId(testId);
		expect(elements.length).toEqual(expected);
	});
	it('it filters out erroneous items', () => {
		render(<ProductSpecification attributes={fakeList}/>);
		const testId = 'specification-object';
		const expected = 1;
		const elements = screen.getAllByTestId(testId);
		expect(elements.length).toEqual(expected);
	});
});
