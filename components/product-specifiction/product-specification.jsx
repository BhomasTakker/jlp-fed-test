import PropTypes from 'prop-types';
import styles from './product-specification.module.scss';
import SpecificationObject from './specification-object/specification-object';

// Pass in as a prop or get from somewhere
const includedItemsFilter = [
	'adjustable racking',
	'child lock',
	'delay start',
	'delicate wash',
	'dimensions',
	'drying performance',
	'drying system',
];

const items = attributes => corruptItemsFilter(attributes).map(item => (
	<SpecificationObject item={item} key={item.name}/>
));

// Utils and unit tests?
// Theres an argument for filtering on the server
// only supply the information you're going to use
const corruptItemsFilter = attributes => attributes.filter(item => item.name && item.value);
const filter = (items, toFilter) => items.filter(item => toFilter.includes(item.name.toLowerCase()));

const ProductSpecification = ({attributes}) => {
	const filteredItems = filter(attributes, includedItemsFilter);

	return (
		<section>
			<h3 className={styles.title}>Product specification</h3>
			<ul className={styles.list}>
				{items(filteredItems)}
			</ul>
		</section>
	);
};

export default ProductSpecification;

ProductSpecification.propTypes = {
	attributes: PropTypes.array.isRequired,
};

