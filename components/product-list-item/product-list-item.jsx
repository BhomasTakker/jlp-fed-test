import styles from './product-list-item.module.scss';
import PropTypes from 'prop-types';
import Image from 'next/image';
import Price from '../price/price';

// This would need protecting
const drawPriceRange = (max, min) => {
	const isPriceRange = max !== min;
	return isPriceRange
		? <div className={styles.price}><Price price={min}/> - <Price price={max}/></div>
		: <div className={styles.price}><Price price={min}/></div>;
};

// This should control the actual link
const ProductListItem = ({image, price, title}) => {
	const {max, min} = price.value;

	return (

		<div className={styles.content} data-testid='list-item'>
			<div className={styles.imageContainer}>
				{/* Shouldn't use description for alt text - find the correct */}
				<Image className={styles.image} src={`https:${image}`} alt={title} width={480} height={640} layout='intrinsic' objectFit={'contain'} />
			</div>
			<div className={styles.title}>{title}</div>
			{drawPriceRange(max, min)}
		</div>
	);
};

ProductListItem.propTypes = {
	image: PropTypes.string.isRequired,
	price: PropTypes.object.isRequired,
	title: PropTypes.string.isRequired,
};

export default ProductListItem;
