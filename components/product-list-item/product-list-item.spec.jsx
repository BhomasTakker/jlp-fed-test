import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsListData from '../../mockData/data.json';
import ProductListItem from './product-list-item';

// Repeating - should create meaningfull mockData objects
const exampleProduct = productsListData.products[0];

exampleProduct.variantPriceRange = {
	display: {
		max: '£500.00',
		min: '£500.00',
	},
	value: {
		max: '500.00',
		min: '500.00',
	},
};

const {image, title, variantPriceRange} = exampleProduct;
describe('ProductListItem ', () => {
	beforeEach(() => {
		render(<ProductListItem image={image} price={variantPriceRange} title={title} />);
	});
	it('it renders given title', () => {
		const element = screen.getByText(title);

		expect(element).toBeDefined();
	});
	it('it renders given image', () => {
		const role = 'img';
		const element = screen.getByRole(role);

		// Blob
		expect(element).toHaveAttribute('src');
	});

	it('it renders given price in pounds', () => {
		const currency = '£';
		const expected = exampleProduct.variantPriceRange.value.max;
		const element = screen.getByText(`${currency}${expected}`);

		expect(element).toBeDefined();
	});
});
