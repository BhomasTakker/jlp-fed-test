import PropTypes from 'prop-types';
import ProductCarousel from '../product-carousel/product-carousel';
import styles from './product-display.module.scss';

// No longer required
// remove component
const ProductDisplay = ({data}) => {
	const {media} = data;
	const {images} = media;

	return (
		<section>
			<ProductCarousel images={images} />
		</section>
	);
};

export default ProductDisplay;

ProductDisplay.propTypes = {
	data: PropTypes.shape({
		price: PropTypes.object.isRequired,
		displaySpecialOffer: PropTypes.string.isRequired,
		additionalServices: PropTypes.object.isRequired,
		media: PropTypes.object.isRequired,
	}).isRequired,
};
