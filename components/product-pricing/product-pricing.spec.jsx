import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsData from '../../mockData/data2.json';
import ProductPricing from './product-pricing';

const exampleData = productsData.detailsData[5];

const {price, additionalServices} = exampleData;
const {includedServices} = additionalServices;

const specialOffer = 'SALE';
const currency = '£';

describe('ProductPricing ', () => {
	beforeEach(() => {
		render(<ProductPricing price={price} displaySpecialOffer={specialOffer} additionalServices={additionalServices}/>);
	});
	it('it renders price', () => {
		const element = screen.getByText(`${currency}${price.now}`);
		expect(element).toBeDefined();
	});

	it('it renders special offer', () => {
		const element = screen.getByText(specialOffer);
		expect(element).toBeDefined();
	});
	it('it renders services included', () => {
		const element = screen.getByText(includedServices);
		expect(element).toBeDefined();
	});
	// It('it does not render undefined', () => {
	// 	render(<ProductPricing price={undefined} displaySpecialOffer={specialOffer} additionalServices={additionalServices}/>);
	// 	const element = screen.getByText(`${currency}undefined`);
	// 	expect(element).not.toBeDefined();
	// });
	// it('it does not render a pound sign alone', () => {
	// 	render(<ProductPricing price={''} displaySpecialOffer={specialOffer} additionalServices={additionalServices}/>);
	// 	const element = screen.getByText(`${currency}undefined`);
	// 	expect(element).not.toBeDefined();
	// });
	// it('it will only render a number', () => {
	// 	render(<ProductPricing price={'five'} displaySpecialOffer={specialOffer} additionalServices={additionalServices}/>);
	// 	const element = screen.getByText(`${currency}five`);
	// 	expect(element).not.toBeDefined();
	// });
});
