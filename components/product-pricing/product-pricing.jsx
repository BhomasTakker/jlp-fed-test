import PropTypes from 'prop-types';
import Price from '../price/price';
import styles from './product-pricing.module.scss';

const ProductPricing = ({price, displaySpecialOffer, additionalServices}) => {
	const {includedServices} = additionalServices;
	return (
		<section>
			<hgroup>
				<Price price={price.now} />
				<h3 className={styles.offer}>{displaySpecialOffer}</h3>
				<h3 className={styles.service}>{includedServices}</h3>
			</hgroup>
		</section>
	);
};

ProductPricing.propTypes = {
	price: PropTypes.object.isRequired,
	displaySpecialOffer: PropTypes.string.isRequired,
	additionalServices: PropTypes.object.isRequired,
};

export default ProductPricing;
