import styles from './layout.module.scss';
import PropTypes from 'prop-types';

// I perhaps should have added headers here - would be much better suited
// Could use router to get header info or perhaps portals to set???
// I'm sure there's a simple way but I couldn't think of it
// As it is I have removed any relavence

const Layout = ({children}) => (
	<div className={styles.content}>
		<div>{children}</div>
		{/* <main className={styles.main}>
			<div>
				<div>{children}</div>
			</div>
		</main> */}
	</div>
);

Layout.propTypes = {
	children: PropTypes.arrayOf.node,
};

export default Layout;
