import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import Price from './price';

const value1 = 555.55;
const value2 = '555.55';
const value3 = 'five';
const value4 = undefined;
const value5 = 555;
const value6 = 55.555555;
const value7 = '';
const value8 = 0.01;

const renderPrice = price => {
	render(<Price price={price} />);
};

describe('Test suite for price component', () => {
	it('it renders a value with currency symbol', async () => {
		const expected = `£${value1}`;
		renderPrice(value1);
		const element = await screen.findByText(expected);
		expect(element).toBeDefined();
	});

	it('it decimalises a number', async () => {
		const expected = '£555.00';
		renderPrice(value5);
		const element = await screen.findByText(expected);
		// Just checking
		expect(element.textContent).toEqual(expected);
	});
	it('it accepts a valid number string to render', async () => {
		const expected = `£${value2}`;
		renderPrice(value2);
		const element = await screen.findByText(expected);
		expect(element).toBeDefined();
	});
	// Proptypes errors here
	// it converts a valid number string
	// but errors on a non number / rendering isNaN
	it('it will not render an invalid string', async () => {
		const testId = 'invalid-price';
		renderPrice(value3);
		const element = await screen.findByTestId(testId);
		expect(element).not.toHaveValue();
	});

	it('it renders a number to two decimal places', async () => {
		const expected = '£55.56';
		renderPrice(value6);
		const element = await screen.findByText(expected);
		expect(element).toBeDefined();
	});

	it('it does not render NaN ', async () => {
		const testId = 'invalid-price';
		renderPrice(NaN);
		const element = await screen.findByTestId(testId);
		expect(element).not.toHaveValue();
	});

	it('it does not render infinity ', async () => {
		const testId = 'invalid-price';
		renderPrice(Infinity);
		const element = await screen.findByTestId(testId);
		expect(element).not.toHaveValue();
	});

	it('it does not render an empty string ', async () => {
		const testId = 'invalid-price';
		renderPrice(value7);
		const element = await screen.findByTestId(testId);
		expect(element).not.toHaveValue();
	});

	it('it renders a 0 pounds number', async () => {
		const expected = `£${value8}`;
		renderPrice(value8);
		const element = await screen.findByText(expected);
		expect(element).toBeDefined();
	});
});

