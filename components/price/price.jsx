import PropTypes from 'prop-types';
import styles from './price.module.scss';

// Utils and unit tests
const decimalise = val => val.toFixed(2);
const isNum = val => isFinite(val);
const isValidType = val => typeof val === 'string' || typeof val === 'number';
const isNotEmpty = val => val !== '';

const validityChecks = val => isValidType(val) && isNotEmpty(val);

const Price = ({price}) => {
	const currency = '£';
	const display = decimalise(Number(price));

	if (!isNum(display) || !validityChecks(price)) {
		return (<h2 className={styles.price} data-testid='invalid-price'></h2>);
	}

	return (<h2 className={styles.price}>{`${currency}${display}`}</h2>);
};

export default Price;

Price.propTypes = {
	price: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string,
	]),
};
