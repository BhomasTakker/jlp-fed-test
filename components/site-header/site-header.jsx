import Head from 'next/head';
import PropTypes from 'prop-types';

const SiteHeader = ({pageTitle = 'Home'}) => (
	<Head>
		<title>{`JL & Partners | ${pageTitle}`}</title>

		<meta name='keywords' content='shopping' />
	</Head>
);

SiteHeader.propTypes = {
	pageTitle: PropTypes.string,
};

export default SiteHeader;
