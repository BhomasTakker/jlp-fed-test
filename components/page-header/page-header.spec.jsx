import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsData from '../../mockData/data2.json';
import PageHeader, {Nav} from './page-header';
import {mockUseRouter} from '../../__mocks__';

const exampleData = productsData.detailsData[0];
const title = 'test';
jest.mock('next/router', () => ({
	useRouter() {
		return mockUseRouter();
	},
}));
// Expect(queryByRole('timestamp')).not.toBeInTheDocument();
describe('Page Header', () => {
	beforeEach(() => {
		render(<PageHeader title={title} />);
	});
	it('Displays title ', () => {
		expect(screen.getByText(title)).toBeDefined();
	});
	it('Does not display Nav on home ', () => {
		expect(screen.queryByRole('button')).not.toBeInTheDocument();
	});
	it('Does display Nav on home ', () => {
		jest.mock('next/router', () => ({
			useRouter() {
				return mockUseRouter('/dishwasher');
			},
		}));
		expect(screen.queryByRole('button')).toBeDefined();
	});
});

describe('Nav Component', () => {
	beforeEach(() => {
		render(<Nav />);
	});
	// If/when button / test by role
	it.todo('Nav test - really unsure here!');
	// Test user click and router.back called once
});
