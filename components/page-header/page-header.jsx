import Link from 'next/link';
import PropTypes from 'prop-types';
import styles from './page-header.module.scss';
import {useRouter} from 'next/router';

export const Nav = () =>
	(
		<nav>
			<Link
				href={{
					pathname: '/',
				}}>
				<a className={styles.link}>
					<div className={styles.back} role='button'></div>
				</a>
			</Link>
		</nav>
	);
const PageHeader = ({title}) => {
	const router = useRouter();
	const isHome = router.pathname === '/';
	return (
		<header className={styles.header}>
			{!isHome && <Nav />}
			<div>
				<h1 className={styles.title}>{title}</h1>
			</div>
		</header>
	);
};

export default PageHeader;

PageHeader.propTypes = {
	title: PropTypes.string.isRequired,
};
