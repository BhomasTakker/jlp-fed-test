import DOMPurify from 'isomorphic-dompurify';
import PropTypes from 'prop-types';
import {useState} from 'react';
import styles from './product-information.module.scss';

// Should probably split this up
const ProductInformation = ({productInformation, productCode}) => {
	const purifiedProductInformation = DOMPurify.sanitize(productInformation);
	const [isShowingMore, setIsShowingMore] = useState(false);
	// Need to clean productInformation
	const onClickHandler = () => {
		setIsShowingMore(!isShowingMore);
	};

	const showMoreStyle = isShowingMore ? styles.infoTextMore : styles.infoTextLess;
	const labelText = isShowingMore ? 'Less' : 'Read more';
	const infoClass = isShowingMore ? styles.lessInfo : styles.moreInfo;

	return (
		<section className={styles.contents}>
			<article className={styles.info}>
				<h3>Product information</h3>
				<div className={styles.container}>
					<div className={showMoreStyle} dangerouslySetInnerHTML={{__html: purifiedProductInformation}} />
					<aside>
						<p className={styles.code}>{`Product code: ${productCode}`}</p>
					</aside>
				</div>
			</article>
			{/* Compoenent? */}
			<div className={styles.more} onClick={onClickHandler} role='button'>
				<h3><strong>{labelText}</strong></h3>
				<h3><div className={infoClass}></div></h3>
			</div>
		</section>
	);
};

export default ProductInformation;

ProductInformation.propTypes = {
	productInformation: PropTypes.string.isRequired,
	productCode: PropTypes.string.isRequired,
};

