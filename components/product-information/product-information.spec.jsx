import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import productsData from '../../mockData/data2.json';
import ProductInformation from './product-information';
import DOMPurify from 'isomorphic-dompurify';

const exampleData = productsData;
const detailsData = exampleData.detailsData[0];

// Spy can do both
jest.mock('isomorphic-dompurify', () => ({sanitize: txt => txt}));
const spy = jest.spyOn(DOMPurify, 'sanitize');

describe('ProductInformation ', () => {
	beforeEach(() => {
		const {details} = detailsData;
		render(<ProductInformation productInformation={details.productInformation}/>);
	});
	afterEach(() => {
		spy.mockClear();
	});
	it('Check product code', () => {
		const {code} = exampleData;

		const element = screen.getByText(`Product code: ${code}`);
		expect(element).toBeDefined();
	});

	it('Check sanitize removes harmful html', () => {
		expect(spy).toHaveBeenCalledTimes(1);
	});
	it.todo('Show more on click');
});
